import React from "react";
import {
  Container,
  Box,
  Text,
  Tabs,
  TabList,
  Tab,
  TabPanels,
  TabPanel,
} from "@chakra-ui/react";
import SignIn from "../components/authentication/SignIn";
import Register from "../components/authentication/Register";

const HomePage = () => {
  return (
    <Container maxW="xl" centerContent>
      <Box
        d="flex"
        justifyContent="center"
        p={3}
        bg={"white"}
        w="100%"
        m="40px 0 15px 0"
      >
        <Text fontSize="4xl" fontFamily="Poppins" color="black">
          Mern Chat App
        </Text>
      </Box>
      <Box bg="white" w="100%" p={4} color="black"></Box>
      <Tabs>
        <TabList mb="1em">
          <Tab width="50%">Sign In</Tab>
          <Tab width="50%">Register</Tab>
        </TabList>

        <TabPanels>
          <TabPanel>
            <SignIn />
          </TabPanel>
          <TabPanel>
            <Register />
          </TabPanel>
        </TabPanels>
      </Tabs>
    </Container>
  );
};

export default HomePage;
